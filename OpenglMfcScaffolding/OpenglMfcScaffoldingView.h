
// OpenglMfcScaffoldingView.h : COpenglMfcScaffoldingView クラスのインターフェイス
//

#pragma once


class COpenglMfcScaffoldingView : public CView
{
protected: // シリアル化からのみ作成します。
	COpenglMfcScaffoldingView();
	DECLARE_DYNCREATE(COpenglMfcScaffoldingView)

// 属性
public:
	COpenglMfcScaffoldingDoc* GetDocument() const;

// 操作
public:

// オーバーライド
public:
	virtual void OnDraw(CDC* pDC);  // このビューを描画するためにオーバーライドされます。
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// 実装
public:
	virtual ~COpenglMfcScaffoldingView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成された、メッセージ割り当て関数
protected:
	DECLARE_MESSAGE_MAP()

private:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

	void SetDCPixelFormat(HDC hdc);

	///ClientDC
	std::shared_ptr<CDC> m_pDC;
	///rendering context
	HGLRC m_hGlRC;
};

#ifndef _DEBUG  // OpenglMfcScaffoldingView.cpp のデバッグ バージョン
inline COpenglMfcScaffoldingDoc* COpenglMfcScaffoldingView::GetDocument() const
   { return reinterpret_cast<COpenglMfcScaffoldingDoc*>(m_pDocument); }
#endif

