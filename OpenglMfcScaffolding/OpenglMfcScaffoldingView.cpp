
// OpenglMfcScaffoldingView.cpp : COpenglMfcScaffoldingView クラスの実装
//

#include "stdafx.h"
// SHARED_HANDLERS は、プレビュー、縮小版、および検索フィルター ハンドラーを実装している ATL プロジェクトで定義でき、
// そのプロジェクトとのドキュメント コードの共有を可能にします。
#ifndef SHARED_HANDLERS
#include "OpenglMfcScaffolding.h"
#endif

#include "OpenglMfcScaffoldingDoc.h"
#include "OpenglMfcScaffoldingView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// COpenglMfcScaffoldingView

IMPLEMENT_DYNCREATE(COpenglMfcScaffoldingView, CView)

BEGIN_MESSAGE_MAP(COpenglMfcScaffoldingView, CView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// COpenglMfcScaffoldingView コンストラクション/デストラクション

COpenglMfcScaffoldingView::COpenglMfcScaffoldingView()
{
	// TODO: 構築コードをここに追加します。

}

COpenglMfcScaffoldingView::~COpenglMfcScaffoldingView()
{
}

BOOL COpenglMfcScaffoldingView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: この位置で CREATESTRUCT cs を修正して Window クラスまたはスタイルを
	//  修正してください。

	return CView::PreCreateWindow(cs);
}

// COpenglMfcScaffoldingView 描画

void COpenglMfcScaffoldingView::OnDraw(CDC* /*pDC*/)
{
	COpenglMfcScaffoldingDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: この場所にネイティブ データ用の描画コードを追加します。
	SwapBuffers(m_pDC->m_hDC);
}


// COpenglMfcScaffoldingView 診断

#ifdef _DEBUG
void COpenglMfcScaffoldingView::AssertValid() const
{
	CView::AssertValid();
}

void COpenglMfcScaffoldingView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

void COpenglMfcScaffoldingView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	// TODO: ここにメッセージ ハンドラー コードを追加します。
	glViewport(0, 0, cx, cy);
}

int COpenglMfcScaffoldingView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO: ここに特定な作成コードを追加してください。
	try
	{
		m_pDC = std::make_shared<CClientDC>(this);
		SetDCPixelFormat(m_pDC->GetSafeHdc());
		m_hGlRC = ::wglCreateContext(m_pDC->GetSafeHdc());
		wglMakeCurrent(m_pDC->GetSafeHdc(), m_hGlRC);

		glClearColor(0.0f, 0.2f, 0.4f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
	}
	catch (const std::exception& e)
	{
		CString value(e.what());
		::AfxMessageBox(value, MB_OK | MB_ICONERROR);
		SendMessage(WM_CLOSE);
	}
	return 0;
}

void COpenglMfcScaffoldingView::SetDCPixelFormat(HDC hdc)
{
	//ピクセルフォーマットのデスクリプタ
	//マルチサンプリングをする場合はウィンドウの物を使うので、この値は上書きされる
	PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),         // Specifies the size of this data structure
		1,                                      // Specifies the version of this data structure
		PFD_DRAW_TO_WINDOW |                    // ピクセルバッファのビットフラグの設定
		PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,                        // RGBA pixel values
		32,                                   // 32-bitカラーと指定
		0, 0, 0, 0, 0, 0,                     // Specifies the number of red bitplanes in each RGBA color buffer
		0, 0,                                 // Specifies the number of alpha bitplanes in each RGBA color buffer
		0, 0, 0, 0, 0,                        // Specifies the total number of bitplanes in the accumulation buffer
		32,                                   // Specifies the depth(bit) of the depth (z-axis) buffer
		32,                                   // Specifies the depth of the stencil buffer
		0,                                    // Specifies the number of auxiliary buffers
		PFD_MAIN_PLANE,                       // Layer type　Ignored...
		0,                                    // Specifies the number of overlay and underlay planes
		0,                                    // Ignored
		0,                                    // Specifies the transparent color or index of an underlay plane
		0                                     // Ignored
	};

	//pixelフォーマット
	int pixelFormatIndex = ChoosePixelFormat(hdc, &pfd);

	//実際のDCにピクセルフォーマットを設定する
	if (!SetPixelFormat(hdc, pixelFormatIndex, &pfd))
	{
		throw std::runtime_error("Failed to set pixel format");
	}

	if (DescribePixelFormat(hdc, pixelFormatIndex, sizeof(PIXELFORMATDESCRIPTOR), &pfd) == 0)
	{
		throw std::runtime_error("the device context's pixel format is invalid");
	}
}

COpenglMfcScaffoldingDoc* COpenglMfcScaffoldingView::GetDocument() const // デバッグ以外のバージョンはインラインです。
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(COpenglMfcScaffoldingDoc)));
	return (COpenglMfcScaffoldingDoc*)m_pDocument;
}
#endif //_DEBUG


