
// OpenglMfcScaffolding.h : OpenglMfcScaffolding アプリケーションのメイン ヘッダー ファイル
//
#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"       // メイン シンボル


// COpenglMfcScaffoldingApp:
// このクラスの実装については、OpenglMfcScaffolding.cpp を参照してください。
//

class COpenglMfcScaffoldingApp : public CWinApp
{
public:
	COpenglMfcScaffoldingApp();


// オーバーライド
public:
	virtual BOOL InitInstance();

// 実装
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern COpenglMfcScaffoldingApp theApp;
