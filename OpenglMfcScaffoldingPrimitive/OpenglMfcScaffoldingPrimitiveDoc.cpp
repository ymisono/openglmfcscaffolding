
// OpenglMfcScaffoldingPrimitiveDoc.cpp : COpenglMfcScaffoldingPrimitiveDoc クラスの実装
//

#include "stdafx.h"
// SHARED_HANDLERS は、プレビュー、縮小版、および検索フィルター ハンドラーを実装している ATL プロジェクトで定義でき、
// そのプロジェクトとのドキュメント コードの共有を可能にします。
#ifndef SHARED_HANDLERS
#include "OpenglMfcScaffoldingPrimitive.h"
#endif

#include "OpenglMfcScaffoldingPrimitiveDoc.h"

#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// COpenglMfcScaffoldingPrimitiveDoc

IMPLEMENT_DYNCREATE(COpenglMfcScaffoldingPrimitiveDoc, CDocument)

BEGIN_MESSAGE_MAP(COpenglMfcScaffoldingPrimitiveDoc, CDocument)
END_MESSAGE_MAP()


// COpenglMfcScaffoldingPrimitiveDoc コンストラクション/デストラクション

COpenglMfcScaffoldingPrimitiveDoc::COpenglMfcScaffoldingPrimitiveDoc()
{
	// TODO: この位置に 1 度だけ呼ばれる構築用のコードを追加してください。

}

COpenglMfcScaffoldingPrimitiveDoc::~COpenglMfcScaffoldingPrimitiveDoc()
{
}

BOOL COpenglMfcScaffoldingPrimitiveDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: この位置に再初期化処理を追加してください。
	// (SDI ドキュメントはこのドキュメントを再利用します。

	return TRUE;
}




// COpenglMfcScaffoldingPrimitiveDoc シリアル化

void COpenglMfcScaffoldingPrimitiveDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 格納するコードをここに追加してください。
	}
	else
	{
		// TODO: 読み込むコードをここに追加してください。
	}
}

#ifdef SHARED_HANDLERS

//縮小版のサポート
void COpenglMfcScaffoldingPrimitiveDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// このコードを変更してドキュメントのデータを描画します
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// 検索ハンドラーのサポート
void COpenglMfcScaffoldingPrimitiveDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// ドキュメントのデータから検索コンテンツを設定します。
	// コンテンツの各部分は ";" で区切る必要があります

	// 例:      strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void COpenglMfcScaffoldingPrimitiveDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// COpenglMfcScaffoldingPrimitiveDoc 診断

#ifdef _DEBUG
void COpenglMfcScaffoldingPrimitiveDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void COpenglMfcScaffoldingPrimitiveDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// COpenglMfcScaffoldingPrimitiveDoc コマンド
