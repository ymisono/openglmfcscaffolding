
// OpenglMfcScaffoldingPrimitiveView.cpp : COpenglMfcScaffoldingPrimitiveView クラスの実装
//

#include "stdafx.h"
// SHARED_HANDLERS は、プレビュー、縮小版、および検索フィルター ハンドラーを実装している ATL プロジェクトで定義でき、
// そのプロジェクトとのドキュメント コードの共有を可能にします。
#ifndef SHARED_HANDLERS
#include "OpenglMfcScaffoldingPrimitive.h"
#endif

#include "OpenglMfcScaffoldingPrimitiveDoc.h"
#include "OpenglMfcScaffoldingPrimitiveView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// COpenglMfcScaffoldingPrimitiveView

IMPLEMENT_DYNCREATE(COpenglMfcScaffoldingPrimitiveView, CView)

BEGIN_MESSAGE_MAP(COpenglMfcScaffoldingPrimitiveView, CView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// COpenglMfcScaffoldingPrimitiveView コンストラクション/デストラクション

COpenglMfcScaffoldingPrimitiveView::COpenglMfcScaffoldingPrimitiveView()
{
	// TODO: 構築コードをここに追加します。

}

COpenglMfcScaffoldingPrimitiveView::~COpenglMfcScaffoldingPrimitiveView()
{
	glDeleteVertexArrays(1, &m_vao);
	glDeleteBuffers(1, &m_vbo);
	glDeleteProgram(m_shaderProgram);
}

BOOL COpenglMfcScaffoldingPrimitiveView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: この位置で CREATESTRUCT cs を修正して Window クラスまたはスタイルを
	//  修正してください。

	return CView::PreCreateWindow(cs);
}

// COpenglMfcScaffoldingPrimitiveView 描画

void COpenglMfcScaffoldingPrimitiveView::OnDraw(CDC* /*pDC*/)
{
	COpenglMfcScaffoldingPrimitiveDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: この場所にネイティブ データ用の描画コードを追加します。
	glClearColor(0.0f, 0.2f, 0.4f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(m_shaderProgram);
	glBindVertexArray(m_vao);
		glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindVertexArray(0);

	SwapBuffers(m_pDC->m_hDC);
}

void COpenglMfcScaffoldingPrimitiveView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	// TODO: ここにメッセージ ハンドラー コードを追加します。
	glViewport(0, 0, cx, cy);
}

int COpenglMfcScaffoldingPrimitiveView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO: ここに特定な作成コードを追加してください。
	try
	{
		//OpenGLコンテキスト
		m_pDC = std::make_shared<CClientDC>(this);
		SetDCPixelFormat(m_pDC->GetSafeHdc());
		m_hGlRC = ::wglCreateContext(m_pDC->GetSafeHdc());
		wglMakeCurrent(m_pDC->GetSafeHdc(), m_hGlRC);

		//glew
		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK)
		{
			throw std::runtime_error("Failed to initialize GLEW");
		}

		//オブジェクト構築
		GLfloat vertices[] = {
			-0.5f, -0.5f, 0.0f,
			0.5f, -0.5f, 0.0f,
			0.0f,  0.5f, 0.0f
		};
		glGenVertexArrays(1, &m_vao);
		glGenBuffers(1, &m_vbo);
		glBindVertexArray(m_vao);
			glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
				glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
				glEnableVertexAttribArray(0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

		//シェーダー用
		std::ifstream vsfile("basic.vert");
		std::string vsSource;
		std::string buff;
		while (std::getline(vsfile, buff))
		{
			vsSource += buff + std::string("\n");
		}

		std::ifstream fsfile("basic.frag");
		std::string fsSource;
		while (std::getline(fsfile, buff))
		{
			fsSource += buff + std::string("\n");
		}
		m_shaderProgram = CreateShader(vsSource.data(), fsSource.data());
	}
	catch (const std::exception& e)
	{
		CString value(e.what());
		::AfxMessageBox(value, MB_OK | MB_ICONERROR);
		SendMessage(WM_CLOSE);
	}
	return 0;
}

void COpenglMfcScaffoldingPrimitiveView::SetDCPixelFormat(HDC hdc)
{
	//ピクセルフォーマットのデスクリプタ
	//マルチサンプリングをする場合はウィンドウの物を使うので、この値は上書きされる
	PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),         // Specifies the size of this data structure
		1,                                      // Specifies the version of this data structure
		PFD_DRAW_TO_WINDOW |                    // ピクセルバッファのビットフラグの設定
		PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,                        // RGBA pixel values
		32,                                   // 32-bitカラーと指定
		0, 0, 0, 0, 0, 0,                     // Specifies the number of red bitplanes in each RGBA color buffer
		0, 0,                                 // Specifies the number of alpha bitplanes in each RGBA color buffer
		0, 0, 0, 0, 0,                        // Specifies the total number of bitplanes in the accumulation buffer
		32,                                   // Specifies the depth(bit) of the depth (z-axis) buffer
		32,                                   // Specifies the depth of the stencil buffer
		0,                                    // Specifies the number of auxiliary buffers
		PFD_MAIN_PLANE,                       // Layer type　Ignored...
		0,                                    // Specifies the number of overlay and underlay planes
		0,                                    // Ignored
		0,                                    // Specifies the transparent color or index of an underlay plane
		0                                     // Ignored
	};

	//pixelフォーマット
	int pixelFormatIndex = ChoosePixelFormat(hdc, &pfd);

	//実際のDCにピクセルフォーマットを設定する
	if (!SetPixelFormat(hdc, pixelFormatIndex, &pfd))
	{
		throw std::runtime_error("Failed to set pixel format");
	}

	if (DescribePixelFormat(hdc, pixelFormatIndex, sizeof(PIXELFORMATDESCRIPTOR), &pfd) == 0)
	{
		throw std::runtime_error("the device context's pixel format is invalid");
	}
}

GLuint COpenglMfcScaffoldingPrimitiveView::CreateShader(const GLchar * vsSource, const GLchar * fsSource) const
{
	const size_t BuffSize = 8192;

	//VS
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vsSource, NULL);
	glCompileShader(vertexShader);
	{
		GLint success;
		GLchar infoLog[BuffSize] = "";
		glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(vertexShader, BuffSize, NULL, infoLog);
			throw std::runtime_error(std::string("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n") + std::string(infoLog));
		}
	}

	//FS
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fsSource, NULL);
	glCompileShader(fragmentShader);
	{
		GLint success;
		GLchar infoLog[BuffSize] = "";
		glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(fragmentShader, BuffSize, NULL, infoLog);
			throw std::runtime_error(std::string("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n") + std::string(infoLog));
		}
	}

	//Program
	GLuint shader = glCreateProgram();
	glAttachShader(shader, vertexShader);
	glAttachShader(shader, fragmentShader);
	glLinkProgram(shader);
	{
		GLint success;
		GLchar infoLog[BuffSize] = "";
		glGetProgramiv(shader, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(shader, BuffSize, NULL, infoLog);
			throw std::runtime_error("ERROR::SHADER::PROGRAM::LINKING_FAILED" + std::string(infoLog));
		}
	}
	//もう使わない
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	return shader;
}

// COpenglMfcScaffoldingPrimitiveView 診断

#ifdef _DEBUG
void COpenglMfcScaffoldingPrimitiveView::AssertValid() const
{
	CView::AssertValid();
}

void COpenglMfcScaffoldingPrimitiveView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

COpenglMfcScaffoldingPrimitiveDoc* COpenglMfcScaffoldingPrimitiveView::GetDocument() const // デバッグ以外のバージョンはインラインです。
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(COpenglMfcScaffoldingPrimitiveDoc)));
	return (COpenglMfcScaffoldingPrimitiveDoc*)m_pDocument;
}
#endif //_DEBUG


// COpenglMfcScaffoldingPrimitiveView メッセージ ハンドラー
