
// OpenglMfcScaffoldingPrimitive.h : OpenglMfcScaffoldingPrimitive アプリケーションのメイン ヘッダー ファイル
//
#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"       // メイン シンボル


// COpenglMfcScaffoldingPrimitiveApp:
// このクラスの実装については、OpenglMfcScaffoldingPrimitive.cpp を参照してください。
//

class COpenglMfcScaffoldingPrimitiveApp : public CWinApp
{
public:
	COpenglMfcScaffoldingPrimitiveApp();


// オーバーライド
public:
	virtual BOOL InitInstance();

// 実装
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern COpenglMfcScaffoldingPrimitiveApp theApp;
