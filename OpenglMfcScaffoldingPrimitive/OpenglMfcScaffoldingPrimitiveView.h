
// OpenglMfcScaffoldingPrimitiveView.h : COpenglMfcScaffoldingPrimitiveView クラスのインターフェイス
//

#pragma once


class COpenglMfcScaffoldingPrimitiveView : public CView
{
protected: // シリアル化からのみ作成します。
	COpenglMfcScaffoldingPrimitiveView();
	DECLARE_DYNCREATE(COpenglMfcScaffoldingPrimitiveView)

// 属性
public:
	COpenglMfcScaffoldingPrimitiveDoc* GetDocument() const;

// オーバーライド
public:
	virtual void OnDraw(CDC* pDC);  // このビューを描画するためにオーバーライドされます。
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// 実装
public:
	virtual ~COpenglMfcScaffoldingPrimitiveView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// 生成された、メッセージ割り当て関数
protected:
	DECLARE_MESSAGE_MAP()

private:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

	void SetDCPixelFormat(HDC hdc);
	GLuint CreateShader(const GLchar* vsSource, const GLchar* fsSource) const;

	///ClientDC
	std::shared_ptr<CDC> m_pDC;
	///rendering context
	HGLRC m_hGlRC;

	GLuint m_shaderProgram;

	GLuint m_vbo;
	GLuint m_vao;
};

#ifndef _DEBUG  // OpenglMfcScaffoldingPrimitiveView.cpp のデバッグ バージョン
inline COpenglMfcScaffoldingPrimitiveDoc* COpenglMfcScaffoldingPrimitiveView::GetDocument() const
   { return reinterpret_cast<COpenglMfcScaffoldingPrimitiveDoc*>(m_pDocument); }
#endif

