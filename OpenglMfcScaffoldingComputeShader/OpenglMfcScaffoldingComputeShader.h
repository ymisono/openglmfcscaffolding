
// OpenglMfcScaffoldingComputeShader.h : PROJECT_NAME アプリケーションのメイン ヘッダー ファイルです
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"		// メイン シンボル


// COpenglMfcScaffoldingComputeShaderApp:
// このクラスの実装については、OpenglMfcScaffoldingComputeShader.cpp を参照してください
//

class COpenglMfcScaffoldingComputeShaderApp : public CWinApp
{
public:
	COpenglMfcScaffoldingComputeShaderApp();

// オーバーライド
public:
	virtual BOOL InitInstance();

// 実装

	DECLARE_MESSAGE_MAP()
};

extern COpenglMfcScaffoldingComputeShaderApp theApp;
