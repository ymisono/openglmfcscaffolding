
// OpenglMfcScaffoldingComputeShaderDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "OpenglMfcScaffoldingComputeShader.h"
#include "OpenglMfcScaffoldingComputeShaderDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// COpenglMfcScaffoldingComputeShaderDlg ダイアログ



COpenglMfcScaffoldingComputeShaderDlg::COpenglMfcScaffoldingComputeShaderDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_OPENGLMFCSCAFFOLDINGCOMPUTESHADER_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void COpenglMfcScaffoldingComputeShaderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(COpenglMfcScaffoldingComputeShaderDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// COpenglMfcScaffoldingComputeShaderDlg メッセージ ハンドラー

BOOL COpenglMfcScaffoldingComputeShaderDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// このダイアログのアイコンを設定します。アプリケーションのメイン ウィンドウがダイアログでない場合、
	//  Framework は、この設定を自動的に行います。
	SetIcon(m_hIcon, TRUE);			// 大きいアイコンの設定
	SetIcon(m_hIcon, FALSE);		// 小さいアイコンの設定

	// TODO: 初期化をここに追加します。

	return TRUE;  // フォーカスをコントロールに設定した場合を除き、TRUE を返します。
}

// ダイアログに最小化ボタンを追加する場合、アイコンを描画するための
//  下のコードが必要です。ドキュメント/ビュー モデルを使う MFC アプリケーションの場合、
//  これは、Framework によって自動的に設定されます。

void COpenglMfcScaffoldingComputeShaderDlg::OnPaint()
{
	glClearColor(0.0f, 0.2f, 0.4f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	// コンピュートシェーダ
	{
		glUseProgram(m_computeShader);

		// Binding point Indexを設定 (layoutで直接指定していない場合に必要)
		/*
		const GLuint blockIndex = glGetProgramResourceIndex(m_computeShader, GL_SHADER_STORAGE_BLOCK, "SSBO");
		const GLuint blockIndex1 = glGetProgramResourceIndex(m_computeShader, GL_SHADER_STORAGE_BLOCK, "SSBO1");
		const GLuint bindingPointIndex = 1;
		const GLuint bindingPointIndex1 = 2;
		glShaderStorageBlockBinding(m_computeShader, blockIndex, bindingPointIndex);
		glShaderStorageBlockBinding(m_computeShader, blockIndex1, bindingPointIndex1);
		*/

		// SSBO読み込み方法
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_ssbo);
		float buffer[2] = { 0.0f, 0.0f };
		int buffer1 = 0;
		glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(float), &buffer[0]);
		// オフセット指定で同じバッファ内の変数取得を選択（バイト単位）
		glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 4, sizeof(float), &buffer[1]);

		// SSBO読み込み方法2
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_ssbo); // 以前に1回呼ばれていればよい
		auto* readBuffer = static_cast<float*>(glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, sizeof(float) * 2, GL_MAP_READ_BIT));
		glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

		// SSBO読み込み方法 (バッファを切り替える)
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_ssbo1);
		auto* readBuffer1 = static_cast<int*>(glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, sizeof(int), GL_MAP_READ_BIT));
		glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

		// SSBO書き込み方法
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_ssbo); // 以前に1回呼ばれていればよい
		auto* writeBuffer = static_cast<float*>(glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, sizeof(float), GL_MAP_WRITE_BIT));
		*writeBuffer = 1.2f;
		glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

		// コンピュートシェーダーの実行
		// SSBOをシェーダー内のどのバッファに割り当てるかをbinding pointのインデックスで指定
		// メモ：binding pointのインデックスとはシェーダー側のlayer(binding = x)で直接指定、もしくはglShaderStorageBlockBinding()で指定する値
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, m_ssbo);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, m_ssbo1);
		glDispatchCompute(1, 1, 1); // シェーダーを実行
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT); // シェーダーの実行後に読み込む予定がある場合、シェーダー実行が終わるまでロックする

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_ssbo); // 以前に1回呼ばれていればよい
		glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(float), &buffer[0]);
		glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 4, sizeof(float), &buffer[1]);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_ssbo1); // 以前に1回呼ばれていればよい
		glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(int), &buffer1);


		int breakpoint = 0;
	}


	glUseProgram(m_shaderProgram);
	glBindVertexArray(m_vao);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindVertexArray(0);

	SwapBuffers(m_pDC->m_hDC);
}

// ユーザーが最小化したウィンドウをドラッグしているときに表示するカーソルを取得するために、
//  システムがこの関数を呼び出します。
HCURSOR COpenglMfcScaffoldingComputeShaderDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void COpenglMfcScaffoldingComputeShaderDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	glViewport(0, 0, cx, cy);
}

int COpenglMfcScaffoldingComputeShaderDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO: ここに特定な作成コードを追加してください。
	try
	{
		//OpenGLコンテキスト
		m_pDC = std::make_shared<CClientDC>(this);
		SetDCPixelFormat(m_pDC->GetSafeHdc());
		m_hGlRC = ::wglCreateContext(m_pDC->GetSafeHdc());
		wglMakeCurrent(m_pDC->GetSafeHdc(), m_hGlRC);

		//glew
		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK)
		{
			throw std::runtime_error("Failed to initialize GLEW");
		}

		//オブジェクト構築
		GLfloat vertices[] = {
			-0.5f, -0.5f, 0.0f,
			0.5f, -0.5f, 0.0f,
			0.0f,  0.5f, 0.0f
		};
		glGenVertexArrays(1, &m_vao);
		glGenBuffers(1, &m_vbo);
		glBindVertexArray(m_vao);
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

		//シェーダー用
		std::ifstream vsfile("basic.vert");
		std::string vsSource;
		std::string buff;
		while (std::getline(vsfile, buff))
		{
			vsSource += buff + std::string("\n");
		}

		std::ifstream fsfile("basic.frag");
		std::string fsSource;
		while (std::getline(fsfile, buff))
		{
			fsSource += buff + std::string("\n");
		}

		m_shaderProgram = CreateShader(vsSource.data(), fsSource.data());

		//CS
		{
			const int BuffSize = 1024;
			std::ifstream csfile("test.comp");
			std::string csSource;
			while (std::getline(csfile, buff))
			{
				csSource += buff + "\n";
			}
			const GLchar* csSourceChar = static_cast<const GLchar*>(csSource.data());
			GLuint computeShader = glCreateShader(GL_COMPUTE_SHADER);
			glShaderSource(computeShader, 1, (&csSourceChar), NULL);
			glCompileShader(computeShader);
			{
				GLint success;
				GLchar infoLog[BuffSize] = "";
				glGetShaderiv(computeShader, GL_COMPILE_STATUS, &success);
				if (!success)
				{
					glGetShaderInfoLog(computeShader, BuffSize, NULL, infoLog);
					throw std::runtime_error(std::string("ERROR::SHADER::COMPUTE::COMPILATION_FAILED\n") + std::string(infoLog));
				}
			}

			m_computeShader = glCreateProgram();
			glAttachShader(m_computeShader, computeShader);
			glLinkProgram(m_computeShader);
			{
				GLint success;
				GLchar infoLog[BuffSize] = "";
				glGetProgramiv(m_computeShader, GL_LINK_STATUS, &success);
				if (!success)
				{
					glGetProgramInfoLog(m_computeShader, BuffSize, NULL, infoLog);
					throw std::runtime_error("ERROR::SHADER::PROGRAM::LINKING_FAILED" + std::string(infoLog));
				}
			}
			//もう使わない
			glDeleteShader(computeShader);
		}

		// SSBO
		{
			glUseProgram(m_computeShader);
			glGenBuffers(1, &m_ssbo);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_ssbo);
			float initialBuff[] = { 1.0f, 2.0f };
			glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float) * 2, &initialBuff, GL_DYNAMIC_COPY);

			glGenBuffers(1, &m_ssbo1);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_ssbo1);
			int buff = 10;
			glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(int), &buff, GL_DYNAMIC_COPY);

			glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
		}


		{
			int work_grp_cnt[3];
			glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &work_grp_cnt[0]);
			glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &work_grp_cnt[1]);
			glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &work_grp_cnt[2]);

			int work_grp_size[3];
			glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &work_grp_size[0]);
			glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &work_grp_size[1]);
			glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &work_grp_size[2]);

			int work_grp_inv;
			glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &work_grp_inv);
		}
	}
	catch (const std::exception& e)
	{
		CString value(e.what());
		::AfxMessageBox(value, MB_OK | MB_ICONERROR);
		SendMessage(WM_CLOSE);
	}
	return 0;
}

void COpenglMfcScaffoldingComputeShaderDlg::SetDCPixelFormat(HDC hdc)
{
	//ピクセルフォーマットのデスクリプタ
	//マルチサンプリングをする場合はウィンドウの物を使うので、この値は上書きされる
	PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),         // Specifies the size of this data structure
		1,                                      // Specifies the version of this data structure
		PFD_DRAW_TO_WINDOW |                    // ピクセルバッファのビットフラグの設定
		PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,                        // RGBA pixel values
		32,                                   // 32-bitカラーと指定
		0, 0, 0, 0, 0, 0,                     // Specifies the number of red bitplanes in each RGBA color buffer
		0, 0,                                 // Specifies the number of alpha bitplanes in each RGBA color buffer
		0, 0, 0, 0, 0,                        // Specifies the total number of bitplanes in the accumulation buffer
		32,                                   // Specifies the depth(bit) of the depth (z-axis) buffer
		32,                                   // Specifies the depth of the stencil buffer
		0,                                    // Specifies the number of auxiliary buffers
		PFD_MAIN_PLANE,                       // Layer type　Ignored...
		0,                                    // Specifies the number of overlay and underlay planes
		0,                                    // Ignored
		0,                                    // Specifies the transparent color or index of an underlay plane
		0                                     // Ignored
	};

	//pixelフォーマット
	int pixelFormatIndex = ChoosePixelFormat(hdc, &pfd);

	//実際のDCにピクセルフォーマットを設定する
	if (!SetPixelFormat(hdc, pixelFormatIndex, &pfd))
	{
		throw std::runtime_error("Failed to set pixel format");
	}

	if (DescribePixelFormat(hdc, pixelFormatIndex, sizeof(PIXELFORMATDESCRIPTOR), &pfd) == 0)
	{
		throw std::runtime_error("the device context's pixel format is invalid");
	}
}

GLuint COpenglMfcScaffoldingComputeShaderDlg::CreateShader(const GLchar * vsSource, const GLchar * fsSource) const
{
	const size_t BuffSize = 8192;

	//VS
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vsSource, NULL);
	glCompileShader(vertexShader);
	{
		GLint success;
		GLchar infoLog[BuffSize] = "";
		glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(vertexShader, BuffSize, NULL, infoLog);
			throw std::runtime_error(std::string("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n") + std::string(infoLog));
		}
	}

	//FS
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fsSource, NULL);
	glCompileShader(fragmentShader);
	{
		GLint success;
		GLchar infoLog[BuffSize] = "";
		glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(fragmentShader, BuffSize, NULL, infoLog);
			throw std::runtime_error(std::string("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n") + std::string(infoLog));
		}
	}

	//Program
	GLuint shader = glCreateProgram();
	glAttachShader(shader, vertexShader);
	glAttachShader(shader, fragmentShader);
	glLinkProgram(shader);
	{
		GLint success;
		GLchar infoLog[BuffSize] = "";
		glGetProgramiv(shader, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(shader, BuffSize, NULL, infoLog);
			throw std::runtime_error("ERROR::SHADER::PROGRAM::LINKING_FAILED" + std::string(infoLog));
		}
	}
	//もう使わない
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	return shader;

}

