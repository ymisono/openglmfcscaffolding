
// OpenglMfcScaffoldingComputeShaderDlg.h : ヘッダー ファイル
//

#pragma once


// COpenglMfcScaffoldingComputeShaderDlg ダイアログ
class COpenglMfcScaffoldingComputeShaderDlg : public CDialogEx
{
// コンストラクション
public:
	COpenglMfcScaffoldingComputeShaderDlg(CWnd* pParent = nullptr);	// 標準コンストラクター

// ダイアログ データ
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_OPENGLMFCSCAFFOLDINGCOMPUTESHADER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV サポート


// 実装
protected:
	HICON m_hIcon;

	// 生成された、メッセージ割り当て関数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

private:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

	void SetDCPixelFormat(HDC hdc);
	GLuint CreateShader(const GLchar* vsSource, const GLchar* fsSource) const;

	///ClientDC
	std::shared_ptr<CDC> m_pDC;
	///rendering context
	HGLRC m_hGlRC;

	GLuint m_shaderProgram;
	GLuint m_computeShader;

	GLuint m_vbo;
	GLuint m_vao;

	GLuint m_ssbo; //Shader Storage Buffer Objects 
	GLuint m_ssbo1;
};
